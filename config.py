import os
from pathlib import Path
import tcidata

STRIPE_SECRET_KEY = ""
SENDINBLUE_SECRET_KEY = ""
DB_HOST = os.environ.get("DB_HOST", "mongodb://localhost:27017/tci")
SERVER_NAME = "http://localhost:3579"
ROOT_PATH = Path(__file__).parent
DATA_PATH = Path(tcidata.__path__[0]).parent
ASSETS_PATH = Path(ROOT_PATH) / "assets"
# TRANSLATION_FILE_PATH = DATA_PATH / 'i18n/en.csv'
TRANSLATION_DIR_PATH = ROOT_PATH / "i18n"
FALLBACK_LANG = "en"
PROFESSIONALS_URL = "http://localhost:8080"
CLIENTS_URL = "http://localhost:8081/#!/{key}"
CREDIT_PRICE = 5
CREDIT_DISCOUNT_PRICE = 4
CREDIT_DISCOUNT_VOLUME = 150
SUBSCRIPTION_CREDIT_COST = 15
AUDIO_CREDIT_COST = 2
VIDEO_CREDIT_COST = 4
STRIPE_PUBLIC_KEY = "pk_test_NWiEiwM3teAiCXrjtx5NX7pz"

try:
    from config_override import *
except ImportError:
    pass
