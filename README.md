Works with Python3.8

## Installation

```
python3.8 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

You may also need to make tcidata and tcicore available to the project:

```
ln -s venv/src/tcidata .
ln -s venv/src/tcicore .
```


or via Docker:

```
make clone-private-repos
docker build --tag tci-api .
docker run --name tci-api -p 8000:8000 tci-api
```
