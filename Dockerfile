FROM python:3.8.19-bullseye

WORKDIR /app

COPY . .

ADD ~/.ssh/id_rsa  /root/.ssh/

RUN apt update && \
    apt install -y git openssh-client wkhtmltopdf && \
    pip install --upgrade pip && \
    pip install -r requirements.txt gunicorn
RUN chmod +x /usr/bin/wkhtmltopdf

ENV WKHTMLTOPDF_PATH=/usr/bin/wkhtmltopdf

CMD ["gunicorn", "src:app", "-b", "0.0.0.0", "--worker-class", "roll.worker.Worker", "--timeout", "60", "-w", "12"]
