serve:
	WKHTMLTOPDF_PATH=`which wkhtmltopdf` hupper -m server

run:
	. venv/bin/activate gunicorn server --timeout 60

clone_private_repos:
	git clone git@gitlab.com:anthropedia/tci/core.git /tmp/tcicore
	git clone git@gitlab.com:anthropedia/tci/data.git /tmp/tcidata
	mv /tmp/tcidata/tcidata /tmp/tcicore/tcicore .

deploy:
	ssh tci 'cd ~/tci/api && \
		git reset --hard HEAD~10 && \
		git pull && \
		source ./venv/bin/activate && \
		pip install -r requirements.txt && \
		sudo systemctl link `pwd`/tci.service && \
		sudo systemctl daemon-reload && \
		make restart_server'

logs:
	ssh tci 'tail /var/log/syslog'

restart_server:
	sudo systemctl restart tci

create_admin:  # name="" email="" password=""
	. venv/bin/activate && python -c """from src.providers.models import Provider; Provider(name="${name}", email="${email}", is_admin=True).set_password("${password}").save()"""

clone_db:
	ssh tci 'cd /tmp/ && mongodump -d tci'
	rsync -avz tci:/tmp/dump/tci dump/
	mongo tci --eval 'db.dropDatabase()'
	mongorestore
