from datetime import datetime

from mongoengine import Document, DictField, ReferenceField, DateTimeField, IntField

from ..providers.models import Provider


class Charge(Document):
    provider = ReferenceField(Provider)
    amount = IntField(min_value=0)
    request = DictField()
    response = DictField()
    creation_date = DateTimeField(default=datetime.now)

    def __repr__(self):
        return f"{self.creation_date} {self.amount} {self.provider}"
