class UnitySeparateness:

    correspondances = (
            ("st3", "ss"),
            ("st4", "st"),
            ("st2", "st"),
            ("st1", "sb"),
            ("st5", "sb"),
            ("sd5", "ts"),
            ("sd1", "tt"),
            ("sd4", "tt"),
            ("sd2", "tb"),
            ("sd3", "tb"),
            ("co4", "ts"),
            ("co5", "tt"),
            ("co2", "tt"),
            ("co3", "tb"),
            ("co1", "tb"),
            ("ps1", "bs"),
            ("ps4", "bt"),
            ("ha1", "bt"),
            ("ps2", "bb"),
            ("ns4", "bb"),
            ("rd2", "bs"),
            ("ns1", "bt"),
            ("rd1", "bt"),
            ("ha3", "bb"),
            ("rd3", "bb"),
        )
    values = {
        'temperament': {
            'deltas': [2, 2.35, 2.6, 2.76, 2.91, 3.08, 3.24, 3.4, 3.64, 4],
            'scores': [-3, -2, -1, 1, 2, 3, 2, 1, -1, -2, -3],
        },
        'character': {
            'deltas': [2, 2.5, 3.5, 4, 4.5],
            'scores': [-3, -2, -1, 1, 2, 3],
        },
    }

    def __init__(self, result):
        self.scores = {pair[0]: self.rawscore_to_unity(pair[0], result.average_rawscores[pair[0]])
                       for pair in self.correspondances}

    def rawscore_to_unity(self, trait, rawscore):
        category = 'character' if trait[:2] in ['sd', 'co', 'st'] else 'temperament'
        deltas = self.values[category]['deltas']
        scores = self.values[category]['scores']
        index = 0
        for floor in deltas:
            if rawscore > floor:
                index += 1
        return scores[index]
