from babel.dates import format_date
from .. import session


def result_integration(score, abbr: str) -> dict:
    subtraits = score.extractor.get_traits_tree()[abbr]
    scores = sorted([score.rawscore_averages[t] for t in subtraits])
    # 4 is the maximum possible diff value and we want to reverse the score.
    # value = 4 - (scores[-1] - scores[0])
    value = scores[-1] - scores[0]
    return {'value': value}


def casify_3d(score_3d: str, cases: list) -> str:
    """
    converts a score_3d like 'rnh' to a case modified version based on `cases`.
    `cases` is a list of modifiers: 'l' for low (lowercase)
    and 'h' for high (uppercase).
    Example: `casify_3d('rnh', ['h', 'l', 'h'])` # RnH
    """
    return ''.join([score_3d[i].lower() if v == 'l' else score_3d[i].upper()
                    for i, v in enumerate(cases)])


def date_format(d):
    return format_date(d, locale=session.language or "en_US")
