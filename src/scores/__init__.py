import config

from ..clients.models import Assignment
from tcicore.tci import TCI, TCIValidity, TCIPerformance

from src.jinja import env
from .jinja_helpers import casify_3d, date_format
from ..jinja import trans

from .models import UnitySeparateness


# Inject Jinja functions to templates
env.globals.update(casify_3d=casify_3d, date_format=date_format, max=max)

TEMPERAMENT_TRAITS = ['ns', 'ha', 'rd', 'ps']

def linearize_rawscore(trait, value):
    return (abs(value - 3) * 2 + 1) if trait[:2] in TEMPERAMENT_TRAITS else value


def _get_score_for_assignment(version, id, user, lang):
    assignment = Assignment.objects.get(id=id)
    if version in Assignment.OPTIONS and version not in assignment.options:
        raise Exception(f"Scoring {version} is not available for this assignment.")
    tci = TCI(
        version="tci-3-240",
        answers=assignment.answers,
        country=assignment.client.culture,
    )
    tci_validity = TCIValidity(
        version="tci-3-240", answers=assignment.answers, times=assignment.times
    )
    tci_performance = TCIPerformance(
        version="tci-3-240", answers=assignment.answers
    )

    def casify(trait, index=0):
        score = tci.average_rawscores[trait]
        return trait[index] if score < 3 else trait[index].upper()

    def sum_subtraits(trait, values):
        total = 0
        for name in values.keys():
            value = values[name]
            if name[:2] == trait:
                total += value
        return total


    # 1D scores
    scores_1d = {}
    qualitatives = ['very_low', 'low', 'average_low', 'average_high',
                    'high', 'very_high']
    tendencies = ['low', 'high']
    traits = ['ns', 'ha', 'rd', 'ps', 'sd', 'co', 'st']
    sorted_rawscores = sorted(tci.average_rawscores, key=lambda x: tci.average_rawscores[x])
    sorted_subtraits = [r for r in sorted_rawscores if len(r) > 2]

    ##
    ## % contribution to dynamics of personality
    ##

    linearized_rawscores = [
        (trait, linearize_rawscore(trait, tci.average_rawscores[trait]))
        for trait in tci.average_rawscores.keys()
    ]

    # traits: total contributions to the dynamics of personality
    total_linearized = sum([value
        for trait, value in linearized_rawscores
        if len(trait) == 2
    ])
    # Percentage contributions for each trait
    trait_contributions = {
        trait: (value / total_linearized) * 100
        for (trait, value) in linearized_rawscores
        if len(trait) == 2
    }

    abs_differences_to_three = {trait: abs(tci.average_rawscores[trait] - 3)
        for trait in tci.average_rawscores.keys()
        if len(trait) == 3
    }

    # avg rawscore for character, or difference from 3 to avg rawscore for temperament
    comparable_values = {subtrait: abs_differences_to_three[subtrait] if subtrait[:2] in TEMPERAMENT_TRAITS else tci.average_rawscores[subtrait]
        for subtrait in abs_differences_to_three
    }

    traits_sums = {trait: sum_subtraits(trait, comparable_values)
        for trait in traits
    }

    subtrait_ratio = {
        subtrait: comparable_values[subtrait] / (traits_sums[subtrait[:2]] if traits_sums[subtrait[:2]] else 0.1)
        for subtrait in sorted_subtraits
    }

    subtrait_contributions = {subtrait: trait_contributions[subtrait[:2]] * subtrait_ratio[subtrait]
        for subtrait in sorted_subtraits
    }

    contributions = {}
    contributions.update(trait_contributions)
    contributions.update(subtrait_contributions)

    # @TODO: character subtraits should not be calculated on linearized scores!


    for trait in traits:
        # qualitative
        delta = 4.0 / len(qualitatives)
        score = tci.average_rawscores[trait]
        index = int((score - 1) / delta) if score < 5 else -1
        # integration
        values = sorted([tci.average_rawscores[t] for t in tci.trait_questions[trait]])
        integration = round(values[-1] - values[0], 1)
        # build dict
        scores_1d[trait] = {
            'qualitative': qualitatives[index],
            'tendency': tendencies[int(score >= 3)],
            'integration': integration,
        }

    # 2D scores
    scores_2d = {
        "nh": f"{casify('ns')}{casify('ha')}",
        "nr": f"{casify('ns')}{casify('rd')}",
        "rh": f"{casify('rd')}{casify('ha')}",
        "sc": f"{casify('sd')}{casify('co')}",
        "st": f"{casify('sd')}{casify('st', 1)}",
        "ct": f"{casify('co')}{casify('st', 1)}",
        "ph": f"{casify('ps')}{casify('ha')}",
        "ps": f"{casify('ps')}{casify('sd')}",
        "sh": f"{casify('sd')}{casify('ha')}",
    }

    # 3D scores
    scores_3d = {
        "rnh": f"{casify('rd')}{casify('ns')}{casify('ha')}",
        "cts": f"{casify('co')}{casify('st', 1)}{casify('sd')}",
        "psh": f"{casify('ps')}{casify('sd')}{casify('ha')}",
    }

    us_score = UnitySeparateness(tci)

    config.lang = lang

    env.filters['_'] = lambda t: trans(t, config.lang, 'api')
    template = env.get_template(f"scores/views/{version}.html")
    return template.render(
        assignment=assignment,
        client=assignment.client,
        result=tci,
        validity=tci_validity,
        performance=tci_performance,
        user=user,
        traits=traits,
        scores_1d=scores_1d,
        contributions=contributions,
        sorted_subtraits=sorted_subtraits,
        scores_2d=scores_2d,
        scores_3d=scores_3d,
        us_score=us_score
    )
