import markdown
from datetime import datetime as dt
from math import floor
from jinja2 import Environment, PackageLoader

from .i18n import get_translator

from . import session

import config


env = Environment(loader=PackageLoader("src", ""))

translators = {}


def absolute_url(url):
    if url[0] != "/":
        url = "/" + url
    return f"{config.SERVER_NAME}{url}"


def trans(text, lang, app):
    """
    this filter must be manually injected from views in order to inject the app.

    """
    if f"{lang}/{app}" not in translators.keys():
        translators[f"{lang}/{app}"] = get_translator(lang, app)
    translator = translators.get(f"{lang}/{app}")
    return translator.translate(text)


def datetime(date):
    dt = date if date and type(date) == dt else dt.strptime(date, "%Y-%m-%dT%H:%M:%S")
    return dt.strftime(dt, "%Y/%m/%d")


env.filters["absolute_url"] = absolute_url
# env.filters["_"] = trans
env.filters["datetime"] = datetime
env.filters["round"] = round
env.filters["floor"] = floor
env.filters["markdown"] = markdown.markdown
