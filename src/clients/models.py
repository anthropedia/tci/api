from datetime import datetime
from roll.http import HttpError
from mongoengine import (
    Document,
    StringField,
    EmailField,
    ListField,
    IntField,
    signals,
    QuerySet,
    DoesNotExist,
    ReferenceField,
    LazyReferenceField,
    DateTimeField,
    PULL,
    CASCADE,
)

from slugify import slugify

from src.document import RestDocument, AuthUser
from src.utils import generate_uuid, generate_password
from src.providers import models


class StatusException(Exception):
    pass


class LengthException(Exception):
    pass


class Assignment(Document, RestDocument):
    OPTIONS = ['3d-plus', 'unity-separateness', 'validity-performance']
    extra_fields = ["creation_date"]
    key = StringField(default=lambda: generate_uuid(6))
    answers = ListField()
    times = ListField()
    status = StringField(default="pending", choices=["pending", "completed"])
    completion_date = DateTimeField()
    provider = ReferenceField("models.Provider", required=True)
    version = StringField(default="tci3-240")
    options = ListField(StringField(choices=OPTIONS))
    # client = LazyReferenceField("Client", reverse_delete_rule=CASCADE)

    def complete(self, answers=[], times=[]):
        if self.status != "pending":
            raise StatusException(
                "complete() can only be called on pending assignments."
            )
        if len(answers) != 240 or (times and len(times) != 240):
            raise LengthException("answers and times must have 240 items.")
        self.status = "completed"
        self.times = times
        self.answers = answers
        self.completion_date = datetime.utcnow()
        self.save()

    def add_option(self, option):
        if option in self.options:
            raise Exception(f"This assignment already has the option {option}")
        self.client.provider.remove_credit(1)
        self.provider.reload()
        self.options.append(option)
        self.save()

    @property
    def client(self):
        try:
            return Client.objects.get(assignments__in=[self.id])
        except HttpError:
            return Client()


class Client(RestDocument, Document, AuthUser):
    use_db_fields = ["creation_date"]
    name = StringField(required=True, unique_with="provider", sparse=True)
    email = EmailField(required=False)
    password = StringField()
    gender = StringField()
    birthyear = IntField(min_value=1920, max_value=2030)
    tags = ListField(StringField())
    language = StringField(max_length=2)
    culture = StringField(max_length=2)
    note = StringField()
    assignments = ListField(ReferenceField(Assignment, reverse_delete_rule=PULL))
    latest_allowed_video = StringField()
    provider = ReferenceField("models.Provider", required=True)
    temporary_password = StringField(min_length=6, max_length=6, required=False)
    knowyourself_series = ListField(StringField())
    vod_access_code = StringField()  # replaced by temporary_password. Remove me in MongoDB

    role = "client"

    @property
    def creation_date(self):
        return self.id.generation_time

    def add_assignment(self):
        self.provider.remove_credit(3)
        assignment = Assignment(provider=self.provider).save()
        self.assignments.append(assignment)
        self.save()
        return assignment
    
    def generate_temporary_password(self):
        self.temporary_password = generate_password(6, with_letters=False)

    def check_password(self, value):
        return self.temporary_password.strip() == value.strip()

    @staticmethod
    def slugify_tags(sender, document):
        document.tags = [slugify(t.strip()) for t in document.tags]

    def __str__(self):
        return str(self.name) or "Anonymous"


signals.pre_save.connect(Client.slugify_tags, sender=Client)
