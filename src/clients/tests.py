from src.unittest import TestCase
from ..providers.models import Provider
from .models import Client, Assignment


class ClientModelTest(TestCase):
    def test_tags_should_be_slugified(self):
        provider = Provider(name="P2", email="p@p.net")
        provider.save()
        client = Client(name="John", provider=provider, tags=[" Whå†?! ", "é l é"])
        client.save()
        self.assertListEqual(client.tags, ["wha", "e-l-e"])


class AssignmentModelTest(TestCase):
    def test_assignment_generates_unique_key(self):
        assignment = Assignment()
        self.assertEqual(len(assignment.key), 6)
        assignment2 = Assignment()
        self.assertNotEqual(assignment.key, assignment2.key)
