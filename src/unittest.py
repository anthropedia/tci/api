from unittest import TestCase as BaseTestCase

from src import db


class TestCase(BaseTestCase):
    def tearDown(self):
        db.drop_database("tci_test")
