from hashlib import md5
from datetime import datetime, date
import secrets
import string

from mongoengine import Document, QuerySet
from mongoengine.fields import ObjectId


def jsonify(value):
    if isinstance(value, ObjectId) \
            or isinstance(value, datetime) \
            or isinstance(value, date):
        value = str(value)
    elif isinstance(value, Document):
        value = value.to_object()
    elif isinstance(value, (list, QuerySet)):
        value = [v.to_object() if hasattr(v, "to_object") else v for v in value]
    elif callable(value):
        value = jsonify(value())
    return value


def generate_uuid(length: int = 16):
    string = md5(str(datetime.now()).encode()).hexdigest()
    if length:
        return string[:length]
    return string


def generate_password(length: int = 10, with_letters: bool = True):
    alphabet: str = (string.ascii_letters if with_letters else "") + string.digits
    return "".join(secrets.choice(alphabet) for i in range(length))
