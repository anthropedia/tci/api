import re
import sys

import requests
import jwt
import pydf
from datetime import datetime, timedelta
from roll import Roll, Request, Response, HttpError
from roll.extensions import cors, options, traceback, logger, static
from mongoengine import NotUniqueError, connect
from tcidata import get_tci

from .payments.models import Charge
from .clients.models import Assignment, Client, LengthException, StatusException
from .providers.models import Provider
from .document import RestDocument
from . import mail
from .utils import generate_password
from .scores import _get_score_for_assignment
from .i18n import get_translator

import config

if "unittest" in sys.argv[0]:
    import config_test as config
else:
    import config


class AnonymousUser:
    def is_authenticated(self):
        return False


class RoiceRequest(Request):
    user = AnonymousUser()


class RoiceResponse(Response):
    def document(self, instance: RestDocument):
        self.json = instance.to_object()

    document = property(None, document)

    def collection(self, instances: RestDocument):
        try:
            self.json = [i.to_object() for i in instances]
        except TypeError as e:
            print(f"Error parsing {instances} to objects")
            raise e

    collection = property(None, collection)

    def html(self, value: str) -> None:
        # Shortcut from a dict to JSON with proper content type.
        self.headers["Content-Type"] = "text/html; charset=utf-8"
        self.body = value

    html = property(None, html)


class Roice(Roll):
    Response = RoiceResponse
    Request = RoiceRequest


app = Roice()
cors(app, "*", methods="*", headers=["*", "Authorization"])
options(app)
traceback(app)
logger(app)
static(app, prefix="/assets/", root="assets")

db = connect(host=config.DB_HOST)

JWT_SECRET = "m0res3kr34!"
JWT_ALGORITHM = "HS256"
JWT_EXP_DELTA_SECONDS = 20


def auth_required(f):
    def wrapper(request, *args, **kwargs):
        token = request.headers.get("AUTHORIZATION")
        if not token:
            raise HttpError(401, "No authentication token was provided.")
        decoded = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        user_id = decoded.get("id")
        if not user_id:
            raise HttpError(401, "Unrecognized or expired token.")
        try:
            request.user = Provider.objects.get(id=user_id)
        except Exception as e:
            try:
                request.user = Client.objects.get(id=user_id)
            except Exception as e:
                raise HttpError(403, "This token does not match an existing user.")
        return f(request, *args, **kwargs)

    return wrapper


@app.route("/ping")
async def ping(request, response):
    response.body = "pong"


@app.route("/config")
@auth_required
async def config_view(request, response):
    keys = [
        "PROFESSIONALS_URL",
        "CLIENTS_URL",
        "CREDIT_PRICE",
        "CREDIT_DISCOUNT_PRICE",
        "CREDIT_DISCOUNT_VOLUME",
        "STRIPE_PUBLIC_KEY",
    ]
    response.json = {key.lower(): getattr(config, key) for key in keys}


@app.route("/reset-password", methods=["POST"])
async def reset_password(request, response):
    post = request.json
    if "email" not in post:
        raise HttpError(400, "Need an email to reset password")
    try:
        provider = Provider.objects.get(email=post["email"])
    except Provider.DoesNotExist:
        raise HttpError(400, "No provider found with that email")
    password = provider.generate_temporary_password()
    provider.save()
    content = f"""
A temporary password was created for you.

Login to your account at {config.PROFESSIONALS_URL}:

email: {provider.email}
password: {password}

Once logged in, you should customize your password by clicking on your name in the top right of the page.

The Anthropedia team
"""
    mail.send(
        provider.email,
        "Log in to your TCI professional account",
        content,
        tag="account",
    )
    response.document = provider


@app.route("/providers")
@auth_required
async def provider_list(request, response):
    if not request.user.is_admin:
        raise HttpError(
            401,
            "You are not allowed to create a new provider. Contact an administrator.",
        )
    response.collection = Provider.objects()


@app.route("/provider/tags")
@auth_required
async def provider_tags(request, response):
    tags = Client.objects(provider=request.user).distinct("tags")
    response.json = tags


@app.route("/provider/{id}")
@auth_required
async def provider(request, response, id):
    response.document = Provider.objects.get(id=id)


@app.route("/provider/{id}", methods=["PATCH"])
@auth_required
async def provider_put(request, response, id):
    provider = Provider.objects.get(id=id)
    provider.modify(**request.json)
    response.document = provider


@app.route("/provider", methods=["POST"])
@auth_required
async def provider_post(request, response):
    if not request.user.is_admin:
        raise HttpError(
            401,
            "You are not allowed to create a new provider. Contact an administrator.",
        )
    values = request.json
    password = generate_password(10)
    provider = Provider(**values).set_password(password)
    provider.save()
    content = f"""
A temporary password was created for you.

Log in to your account at {config.PROFESSIONALS_URL}

email: {provider.email}
password: {password}

Once logged in, you should change your password by clicking on your name at the top right-hand corner of the page.

The Anthropedia team
"""
    mail.send(
        provider.email, "TCI Temporary Password", content, tag="account",
    )
    response.document = provider


@app.route("/clients")
@auth_required
async def clients(request, response):
    qs = {}
    if "tag" in request.query and request.query.get("tag"):
        qs["tags"] = request.query.get("tag")
    clients = Client.objects(provider=request.user.id, **qs)
    if "q" in request.query and request.query.get("q"):
        clients = clients.filter(name=re.compile(f".*{request.query.get('q')}.*", re.IGNORECASE))
    response.collection = clients.select_related()


@app.route("/client/{id}")
@auth_required
async def client_get(request, response, id):
    client = Client.objects.get(id=id).select_related()
    if client.provider != request.user:
        raise HttpError(401, "Only the provider can access his clients.")
    response.document = client


@app.route("/client", methods=["POST"])
@auth_required
async def client_post(request, response):
    values = request.json
    values["provider"] = request.user
    client = Client(**values)
    client.save()
    response.document = client


@app.route("/client/{id}", methods=["DELETE"])
@auth_required
async def client_delete(request, response, id):
    client = Client.objects.get(id=id)
    if client.provider != request.user:
        raise HttpError(401, "Only a client's provider can delete him.")
    client.delete()
    response.status = 204


@app.route("/client/{id}", methods=["PATCH"])
@auth_required
async def client_put(request, response, id):
    values = request.json
    try:
        client = Client.objects.get(id=id).select_related()
        if client.provider != request.user:
            raise HttpError(401, "Only a client's provider can edit him.")
        client.update(**values)
        client.save()
        client.reload()
    except NotUniqueError as e:
        raise HttpError(422, e)
    response.document = client


@app.route("/client/{id}/assignment", methods=["POST"])
@auth_required
async def assignment_post(request, response, id):
    client = Client.objects.get(id=id)
    if client.provider != request.user:
        raise HttpError(401, "Only the client's provider can assign him.")
    client.add_assignment()
    response.document = client


@app.route("/user")
@auth_required
async def user_get(request, response):
    user = request.user
    del user.id
    del user.password
    response.document = user


@app.route("/user", methods=["PATCH"])
@auth_required
async def provider_put(request, response):
    user = request.user
    post = request.json
    user.update(name=post["name"], email=post["email"])
    if "password" in post:
        user.set_password(post["password"])
    user.save()
    user.reload()
    response.document = user


@app.route("/payment", methods=["POST"])
@auth_required
async def payment(request, response):
    post = request.json
    token_id = post.get("token").get("id")
    quantity = int(post.get("quantity"))
    unit_price = (
        config.CREDIT_PRICE
        if quantity < config.CREDIT_DISCOUNT_VOLUME
        else config.CREDIT_DISCOUNT_PRICE
    )
    amount = quantity * unit_price
    provider = request.user

    if Charge.objects(request__id=token_id).count():
        raise HttpError(422)

    charge = Charge(provider=provider, request=post.get("token"), amount=amount).save()

    request = requests.post(
        "https://api.stripe.com/v1/charges",
        auth=(config.STRIPE_SECRET_KEY, ""),
        params={
            "source": token_id,
            "amount": amount * 100,
            "currency": "usd",
            "description": f"{quantity} TCI credits",
            "receipt_email": provider.email,
        },
    )
    try:
        charge.response = request.json()
        charge.save()
    except Exception:
        charge.response = request.content
        charge.save()
        raise HttpError(400, request.content)

    if not charge.response.get("paid"):
        raise HttpError(400, request.content)

    provider.add_credit(quantity)
    response.status = 204


@app.route("/token", methods=["POST"])
async def login(request, response):
    data = request.json
    try:
        user = Provider.objects.get(email=data["email"])
    except HttpError:
        user = Client.objects.get(email=data["email"])
    if not user.check_password(data["password"]):
        raise HttpError(403, {"message": "Please check your email and password"})
    token = jwt.encode({"id": str(user.id)}, JWT_SECRET, JWT_ALGORITHM)
    response.json = token


@app.route("/i18n/{lang}/{app}")
async def i18n_app_get(request, response, lang, app):
    try:
        translator = get_translator(lang, app)
        response.json = dict(translator.get_translations())
    except (FileNotFoundError, ValueError) as e:
        raise HttpError(
            404, f"Can't read translation file for {app} in {lang}, {str(e)}"
        )


@app.route("/assignment/kys", methods=["POST"])
@auth_required
async def assignment_kys_patch(request, response):
    data = request.json
    provider = request.user
    client = Client.objects.get(id=data['client'])
    assert client.provider == provider
    new_kys = set(data['assignments']) - set(client.knowyourself_series)
    audio_count = len([a for a in new_kys if a[0] == 'a'])
    video_count = len([v for v in new_kys if v[0] == 'v'])
    audio_cost = config.AUDIO_CREDIT_COST * audio_count
    video_cost = config.VIDEO_CREDIT_COST * video_count
    provider.remove_credit(audio_cost + video_cost)
    client.knowyourself_series += new_kys
    client.knowyourself_series.sort()
    client.save()
    response.status = 204


@app.route("/assignment/{id}")
@auth_required
async def assignment_get(request, response, id):
    assignment = Assignment.objects.get(id=id)
    response.document = assignment


@app.route("/assignment/{key}/questions")
async def assignment_questions(request, response, key):
    assignment = Assignment.objects.get(key=key, status="pending")
    response.json = get_tci(assignment.version).get("questions")


@app.route("/assignment/{id}/purchase", methods=["POST"])
@auth_required
async def assignment_option(request, response, id):
    assignment = Assignment.objects.get(id=id, status="completed")
    assignment.add_option(request.json["version"])
    response.json = assignment.reload().to_object()


@app.route("/subscription/purchase", methods=["POST"])
@auth_required
async def subscription_professional(request, response):
    provider = request.user
    latest_date = provider.subscription_expiration_date or datetime.now()
    extended_date = latest_date + timedelta(days=365)
    provider.remove_credit(config.SUBSCRIPTION_CREDIT_COST)
    provider.subscription_expiration_date = extended_date
    provider.save()
    response.json = provider.reload().to_object()


@app.route("/assignment/{key}/complete", methods=["POST"])
async def assignment_complete(request, response, key):
    assignment = Assignment.objects.get(key=key)
    post = request.json
    try:
        assignment.complete(answers=post.get("answers"), times=post.get("times"))
    except StatusException as e:
        raise HttpError(422, str(e))
    except LengthException as e:
        raise HttpError(400, str(e))
    content = f"""
{assignment.client} has completed the TCI.

The results are available for download at {config.PROFESSIONALS_URL}.

The Anthropedia team
"""
    mail.send(
        assignment.provider.email,
        f"{assignment.client}'s TCI is complete",
        content,
        tag="info",
    )
    assignment.reload()
    response.document = assignment


@app.route("/assignment/{id}", methods=["DELETE"])
@auth_required
async def assignment_delete(request, response, id):
    assignment = Assignment.objects.get(id=id, status="pending")
    client = assignment.client
    if assignment.provider != request.user:
        raise HttpError(401, "Only the owner can delete his own assignment.")
    assignment.provider.add_credit(3)
    assignment.provider.save()
    assignment.delete()
    client.reload()
    response.document = client


@app.route("/auth/generate-code", methods=["POST"])
async def client_generate_temporary_code(request, response):
    post = request.json
    if "email" not in post:
        raise HttpError(400, "Need an email to send key code")
    try:
        client = Client.objects.get(email=post["email"])
    except HttpError as e:
        raise HttpError(403, "No client found with that email")
    client.generate_temporary_password()
    client.save()
    content = f"""
A temporary code was created for you:
Your authentication code is {client.temporary_password}.

The Anthropedia team
"""
    sent = mail.send(
        client.email,
        "Access the My Anthropedia",
        content,
        tag="account",
    )
    response.json = { "email": client.email, sent: sent }


@app.route("/video/auth/{token}")
async def video_auth_token(request, response, token):
    client = Client.objects.get(id=token)
    # @TODO: filter only the necessary keys.
    response.document = client


@app.route("/score/raw.csv")
@auth_required
async def score_raw(request, response):
    response.headers["Content-Type"] = "application/text; charset=utf-8"
    tag = request.query.get('tag')
    assignments = Assignment.objects.filter(provider=request.user.pk, status="completed")
    if tag:
        assignments = [
            assignment
            for assignment in assignments
            if Client.objects.filter(
                assignments__in=[assignment.id],
                tags__in=[tag]
            ).count()
        ]
    lines = []
    for assignment in assignments:
        line = [
            str(assignment.completion_date),
            str(assignment.client.name if assignment.client.name else ""),
        ]
        line += [str(a) for a in assignment.answers]
        lines.append(','.join(line))
    response.body = "\n".join(lines)


@app.route("/assignment/{id}/score/raw.csv")
async def score_raw(request, response, id):
    response.headers["Content-Type"] = "application/text; charset=utf-8"
    assignment = Assignment.objects.get(id=id)
    line = [
        str(assignment.completion_date),
        str(assignment.client.name),
    ]
    line += [str(a) for a in assignment.answers]

    response.body = ",".join(line)


# Keep in the end as it make other routes bug
# A route for /se/ is hardcoded as the routes work for all languages (en, fr, xx)
# except for se. When that is fixed the hardcoded route can be removed.
@app.route("/se/assignment/{id}/score/{version:[^\.]+}.{extension}")
@app.route("/{lang}/assignment/{id}/score/{version:[^\.]+}.{extension}")
async def score_pdf(request, response, id, version, extension, lang="se"):
    request["language"] = lang
    if extension == ".pdf":
        response.body = await pydf.AsyncPydf().generate_pdf(
            _get_score_for_assignment(version, id, request.user, lang),
            print_media_type=True,
        )
    else:
        response.html = _get_score_for_assignment(version, id, request.user, lang)
