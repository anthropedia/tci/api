from roll import HttpError
from mongoengine.errors import NotUniqueError, DoesNotExist, ValidationError
from mongoengine import QuerySet, EmailField, StringField
from passlib.apps import custom_app_context as plhash

from .utils import jsonify, generate_password


class HTTPQuerySet(QuerySet):
    def get(self, *args, **kwargs):
        try:
            return super().get(*args, **kwargs)
        except DoesNotExist as e:
            raise HttpError(
                404, f"{self.__class__.__name__} object matching query does not exist"
            )
        except ValidationError as e:
            raise HttpError(400, str(e))


class RestDocument:
    extra_fields = []

    meta = {"queryset_class": HTTPQuerySet}

    def _try_save(self, method, *args, **kwargs):
        try:
            return getattr(super(), method)(*args, **kwargs)
        except NotUniqueError as e:
            raise HttpError(422, str(e))

    def save(self, *args, **kwargs):
        self._try_save("save", *args, **kwargs)

    def update(self, *args, **kwargs):
        self._try_save("update", *args, **kwargs)

    @property
    def creation_date(self):
        return self.id.generation_time

    def to_object(self):
        keys = ["id"] + list(self.to_mongo()) + self.extra_fields
        "_id" in keys and keys.remove("_id")
        return {key: jsonify(getattr(self, key)) for key in keys}


class AuthUser:
    email = EmailField(required=True, unique=True, sparse=True)
    password = StringField()

    def generate_temporary_password(self):
        password = generate_password(10)
        self.password = plhash.hash(password)
        return password

    def set_password(self, value):
        self.password = plhash.hash(value)
        self.tmp_password = None
        return self

    def check_password(self, value):
        valid = plhash.verify(value, self.password)
        if not valid:
            valid = plhash.verify(value, self.tmp_password)
        return valid
