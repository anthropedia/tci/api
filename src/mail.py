import json

import requests

import config

default = {
    "tags": ["general"],
    "sender": {"email": "reply@tci.anthropedia.org", "name": "Anthropedia"},
    "replyTo": {"email": "support@anthropedia.org", "name": "Anthropedia Support"},
}


def send(to: str, subject: str, text: str, tag: str=None) -> bool:
    payload = dict(default)
    if tag:
        payload["tags"] = [tag]
    if type(to) is str:
        to = [{"email": to}]
    elif type(to) is dict:
        to = [to]
    payload.update({"to": to, "subject": subject, "textContent": text})
    print(payload)
    try:
        response = requests.request(
            "POST",
            "https://api.sendinblue.com/v3/smtp/email",
            data=json.dumps(payload),
            headers={
                "Content-Type": "application/json",
                "api-key": config.SENDINBLUE_SECRET_KEY,
            },
        )
        return response.ok
    except requests.exceptions.ConnectionError as e:
        print("Can't connect to SendInBlue/Brevo")
        return False
