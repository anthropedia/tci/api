from src.unittest import TestCase
from .models import Provider
from src.clients.models import Assignment, Client


class ProviderModelTest(TestCase):
    def test_create_hashed_password(self):
        provider = Provider(name="John")
        provider.set_password("Hey")
        self.assertEqual(len(provider.password), 120)
        self.assertFalse(provider.check_password("Nope"))
        self.assertTrue(provider.check_password("Hey"))

    def test_add_credit(self):
        provider = Provider(name="John", email="j@doe.com")
        provider.credit = 1
        provider.add_credit(2)
        self.assertEqual(provider.credit, 3)

    def test_remove_credit_limitation(self):
        provider = Provider(name="John", email="j@doe.com")
        provider.credit = 1
        provider.save()
        self.assertRaises(Exception, provider.remove_credit, 2)
        self.assertEqual(provider.credit, 1)


class AssignmentModelTest(TestCase):
    def setUp(self):
        self.provider = Provider(name="John", email="j@doe.com")
        self.provider.add_credit(4)
        client = Client(name="Maria Client", provider=self.provider)
        self.assignment = client.add_assignment()

    def test_assignment_option(self):
        self.assignment.add_option("3dplus")
        self.assertIn("3dplus", self.assignment.options)

    def test_assignment_option_duplicate_error(self):
        self.assignment.add_option("3dplus")
        self.assertRaises(Exception, self.assignment.add_option, "3dplus")

    def test_assignment_credit(self):
        self.assertEqual(self.provider.credit, 1)

    def test_assignment_option_credit(self):
        self.assignment.add_option("3dplus")
        self.assertEqual(self.provider.credit, 0)
