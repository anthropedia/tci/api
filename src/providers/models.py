from mongoengine import (
    Document,
    StringField,
    ListField,
    ReferenceField,
    DateField,
    BooleanField,
    IntField,
    PULL,
)
from src.document import RestDocument, AuthUser
from src.clients import models


class CreditException(Exception):
    pass


class Provider(RestDocument, Document, AuthUser):
    use_db_fields = ["creation_date"]
    name = StringField(required=True, unique=True)
    role = StringField(required=True, default="coach")
    permissions = ListField(default=['survey.assign'])
    credit = IntField(default=0, min_value=0)
    clients = ListField(ReferenceField("models.Client", reverse_delete_rule=PULL))
    is_admin = BooleanField(default=False)
    tmp_password = StringField()
    language = StringField(min_length=2, max_length=2)
    # KnowYourself series media access
    subscription_expiration_date = DateField()

    def add_credit(self, quantity):
        self.credit += int(quantity)
        self.save()

    def remove_credit(self, quantity):
        if self.credit < int(quantity):
            raise CreditException(f"{self.name} cannot spend {quantity}. Credit amount too low.")
        self.credit -= int(quantity)
        self.save()
