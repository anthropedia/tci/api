import config

from csvi18n import Translator


def get_translator(lang, app):
    path = config.TRANSLATION_DIR_PATH / lang / f"{app}.csv"
    fallback_path = None
    if config.FALLBACK_LANG:
        fallback_path = config.TRANSLATION_DIR_PATH / config.FALLBACK_LANG / f"{app}.csv"
    return Translator(path, fallback_path=fallback_path)
