import os
from roll.extensions import simple_server

from src import app

simple_server(app, port=int(os.environ.get("APP_PORT", 3579)))
